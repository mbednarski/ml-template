from distutils.core import setup

setup(name='{{cookiecutter.repo_name}}',
      version='0.0-dev',
      packages=['{{cookiecutter.repo_name}}-app', '{{cookiecutter.repo_name}}-model'],
     )